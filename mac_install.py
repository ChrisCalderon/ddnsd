# This file is part of DDNSd.
# Copyright (C) 2021  Christian Calderon
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact the author via email at calderonchristian73 AT gmail DOT com.
"""A script that installs update_ddns.py along with a plist on Macs."""
import sys
import os
from shutil import copyfile, chown
from os.path import exists, dirname, basename
from dataclasses import dataclass, field, fields, MISSING
from argparse import ArgumentParser
from typing import List
import plistlib

PROG = os.path.basename(sys.argv[0])
PLIST_PATH = '/Library/LaunchDaemons/update_ddns.plist'
EXE_PATH = '/usr/local/libexec/update_ddns.py'


@dataclass(frozen=True)
class ArgVals:
    password: str = field(metadata={'help': 'The password used for the DDNS update service.'})
    host: str = field(metadata={'help': 'The host for the DDNS update.'})
    domain: str = field(metadata={'help': 'The domain for the DDNS update.'})
    interval: int = field(default=86400,
                          metadata={'help': 'The interval between update events, in seconds.'})


def parse_args(argv: List[str]) -> ArgVals:
    parser = ArgumentParser(
        prog=PROG,
        description=__doc__,
    )
    for f in fields(ArgVals):
        args = (
            '-{}'.format(f.name[0].upper()),
            '--{}'.format(f.name.replace('_', '-')),
        )
        kwds = {
            'type': f.type,
            'help': f.metadata['help'],
        }
        if f.default is MISSING:
            kwds['required'] = True
        else:
            kwds['default'] = f.default
            kwds['help'] += ' (default: {!r})'.format(f.default)
        parser.add_argument(*args, **kwds)

    args = parser.parse_args(argv[1:])
    return ArgVals(**vars(args))


def main():
    if sys.platform != 'darwin':
        print('This script is only supported on macOS.')
        sys.exit(1)

    args = parse_args(sys.argv)

    # first put update_ddns.py into /usr/local/libexec
    directory = dirname(EXE_PATH)
    if not exists(directory):
        os.mkdir(directory, mode=0o775)
        chown(directory, user='root', group='wheel')

    copyfile(basename(EXE_PATH), EXE_PATH)
    os.chmod(EXE_PATH, mode=0o774)
    chown(EXE_PATH, user='root', group='wheel')

    data = {
        'Label': 'update_ddns',
        'Program': EXE_PATH,
        'EnvironmentVariables': {
            'DDNS_PASSWORD': args.password,
            'DDNS_HOST': args.host,
            'DDNS_DOMAIN': args.domain
        },
        'StartInterval': args.interval
    }
    # then write the plist file
    with open(PLIST_PATH, 'wb') as f:
        plistlib.dump(data, f)

    os.chmod(PLIST_PATH, mode=0o660)
    chown(PLIST_PATH, user='root', group='wheel')


if __name__ == '__main__':
    main()
