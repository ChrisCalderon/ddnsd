# DDNSd

A dynamic DNS client for Namecheap.

There are two scripts provided, `update_ddns.py` and `mac_install.py`.
The `update_ddns.py` script does the actual work of updating the DDNS with the current IP address.
It expects the following environment variables: DDNS_PASSWORD, DDNS_HOST, and DDNS_DOMAIN.
They should contain the password, host, and domain required by the DDNS service, refer to Namecheap's
documentation for more info.

The `mac_install.py` script installs the `update_ddns.py` script as a launchd daemon. It copies
`update_ddns.py` to `/usr/local/libexec/` and creates a plist file at `/Library/LaunchDaemons` which
launchd parses and uses to run the script periodically. The default interval is once per day.

Here is the help output of the `mac_install.py` script:
```
usage: mac_install.py [-h] -P PASSWORD -H HOST -D DOMAIN [-I INTERVAL]

A script that installs update_ddns.py along with a plist on Macs.

optional arguments:
  -h, --help            show this help message and exit
  -P PASSWORD, --password PASSWORD
                        The password used for the DDNS update service.
  -H HOST, --host HOST  The host for the DDNS update.
  -D DOMAIN, --domain DOMAIN
                        The domain for the DDNS update.
  -I INTERVAL, --interval INTERVAL
                        The interval between update events, in seconds. (default: 86400)
```