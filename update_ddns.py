#!/usr/bin/python3
# This file is part of DDNSd.
# Copyright (C) 2021  Christian Calderon
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Contact the author via email at calderonchristian73 AT gmail DOT com.
import sys
import os
from traceback import format_exc
from os.path import basename
from urllib.request import urlopen, Request
from urllib.error import HTTPError, URLError
from syslog import syslog, openlog, closelog, LOG_ERR, LOG_PERROR, LOG_DAEMON, LOG_INFO
from typing import Optional, Sequence, Dict, NamedTuple


ENV_VARS = 'DDNS_HOST', 'DDNS_PASSWORD', 'DDNS_DOMAIN'
DDNS_URL = 'https://dynamicdns.park-your-domain.com/'
DDNS_PARAMS = 'update?host={DDNS_HOST}&domain={DDNS_DOMAIN}&password={DDNS_PASSWORD}&ip={ip}'
IP_LOOKUP_URL = 'https://ipv4.icanhazip.com'
HEADERS = {'User-Agent': 'update-ddns.py'}  # the default urllib User-Agent is blocked.
TIMEOUT = 2.0


class URLResult(NamedTuple):
    error: bool
    body: Optional[bytes]


def get_url(url: str,
            headers: Dict[str, str],
            timeout: float) -> URLResult:
    """Returns the response payload and an error flag, and logs errors."""
    request = Request(url, headers=headers)
    http_err = 'Error: Bad HTTP response code.\nURL: {}\nCode: {}\n'
    url_err = 'Error: Bad URL provided.\nURL: {}\n'
    generic_err = 'Error: Unhandled exception.\n{}'

    had_err = False
    body = None

    try:
        response = urlopen(request, timeout=timeout)
    except HTTPError as exc:
        had_err = True
        syslog(LOG_ERR, http_err.format(url, exc.code))
    except URLError:
        had_err = True
        syslog(LOG_ERR, url_err.format(url))
    except Exception:
        had_err = True
        syslog(LOG_ERR, generic_err.format(format_exc()))
    else:
        body = response.read()
    finally:
        return URLResult(had_err, body)


def get_vars(env_vars: Sequence[str]) -> Optional[Dict[str, str]]:
    """Loads environment variables, and raises a ValueError if any are missing."""
    result = {k: os.environ.get(k, None) for k in env_vars}
    missing = [k for k, v in result.items() if v is None]

    if missing:
        msg = 'Missing environment variable{}: {{}}'.format('s' if len(missing) > 1 else '')
        syslog(LOG_ERR, msg.format(', '.join(missing)))
        return None
    else:
        return result


def close_and_exit(code: int):
    closelog()
    sys.exit(code)


def main():
    openlog(ident=basename(sys.argv[0]),
            logoption=LOG_PERROR,
            facility=LOG_DAEMON)

    env_vars = get_vars(ENV_VARS)

    if env_vars is None:
        close_and_exit(os.EX_DATAERR)

    syslog(LOG_INFO, 'Starting DDNS update.')
    result = get_url(IP_LOOKUP_URL,
                     HEADERS,
                     TIMEOUT)

    if result.error or not result.body:
        syslog(LOG_ERR, 'IP address lookup failed!')
        close_and_exit(os.EX_UNAVAILABLE)

    env_vars['ip'] = result.body.strip().decode()
    result = get_url(DDNS_URL + DDNS_PARAMS.format(**env_vars),
                     HEADERS,
                     TIMEOUT)
    if result.error:
        syslog(LOG_ERR, 'DDNS update failed!')
        close_and_exit(os.EX_UNAVAILABLE)

    syslog(LOG_INFO, 'DDNS update successful.')
    close_and_exit(0)


if __name__ == '__main__':
    main()
